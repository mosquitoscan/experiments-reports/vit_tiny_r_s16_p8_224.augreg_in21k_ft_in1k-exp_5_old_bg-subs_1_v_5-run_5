
Model info for vit_tiny_r_s16_p8_224.augreg_in21k_ft_in1k
=========================================================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 64 x 112 x 112 
StdConv2dSame                             9408       True      
Identity                                                       
ReLU                                                           
____________________________________________________________________________
                     64 x 64 x 56 x 56   
MaxPool2dSame                                                  
____________________________________________________________________________
                     64 x 192 x 7 x 7    
Conv2d                                    786624     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 576       
Linear                                    111168     True      
Identity                                                       
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 50 x 768       
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 50 x 192       
Linear                                    147648     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Dropout                                                        
Identity                                                       
BatchNorm1d                               384        True      
Dropout                                                        
____________________________________________________________________________
                     64 x 512            
Linear                                    98304      True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 12             
Linear                                    6144       True      
____________________________________________________________________________

Total params: 6,240,640
Total trainable params: 6,240,640
Total non-trainable params: 0

Optimizer used: <function Adam at 0x78a309fdc0d0>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback